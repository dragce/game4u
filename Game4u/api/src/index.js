import { typeDefs } from './graphql-schema';
import { ApolloServer } from 'apollo-server';
var neo4j = require('neo4j-driver');
import { makeAugmentedSchema } from 'neo4j-graphql-js';
import dotenv from 'dotenv';

dotenv.config();

const schema = makeAugmentedSchema({
	typeDefs
});

var driver = neo4j.driver(
	'bolt://localhost:7687',
	neo4j.auth.basic('neo4j', 'luka')
);

const server = new ApolloServer({
	context: { driver },
	schema: schema
});

server.listen(process.env.GRAPHQL_LISTEN_PORT, '0.0.0.0').then(({ url }) => {
	console.log(`GraphQL API ready at ${url}`);
});
