import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import { Router } from "@reach/router";

import Game from "../Game/Game.js";

// import { StoreContainer } from "./store";

const GET_GAMES_BY_SEARCH__AND_PLATFORM = gql`
  query getSearchAndFitlerGamesByPlatform(
    $plat: String = ""
    $name: String = ""
  ) {
    Game(
      filter: {
        AND: [
          { title_contains: $name }
          { platforms: { Platform: { name_contains: $plat } } }
        ]
      }
    ) {
      _id
      title
      released
      price
      quantity
      image
    }
  }
`;

const SearchPlatformFilter = ({ plat, name }) => (
  <Query query={GET_GAMES_BY_SEARCH__AND_PLATFORM} variables={{ plat, name }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      console.log("NAME JE: " + name);

      return (
        <div className="div-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game._id}
              game={game}
              btnVisible={"visible"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default SearchPlatformFilter;
