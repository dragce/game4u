import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import { Router } from "@reach/router";

import Game from "../Game/Game.js";

const ALL_PLATS = gql`
  query ALL_PLATS($title: String = "") {
    Game(filter: { title: $title }) {
      id
      genre
      title
      released
      price
      quantity
      image
      cover
      synopsis
    }
  }
`;

const AllGamesPerPlatform = ({ title }) => (
  <Query query={ALL_PLATS} variables={{ title }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      return (
        <div className="div-similar-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game.id}
              game={game}
              btnVisible={"hidden"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default AllGamesPerPlatform;
