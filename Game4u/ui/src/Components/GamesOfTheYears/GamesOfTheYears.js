import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import { Router } from "@reach/router";

import Game from "../Game/Game.js";

// import { StoreContainer } from "./store";

const GET_GAMES_OF_THE_YEARS = gql`
  query BEST_GAMES($year: Int) {
    Game(
      filter: { AND: [{ released: $year }] }
      first: 1
      orderBy: rating_desc
    ) {
      id
      title
      genre
      price
      cover
      image
      released
      rating
      quantity
      synopsis
    }
  }
`;

const GamesOfTheYears = ({ year, fun }) => (
  <Query query={GET_GAMES_OF_THE_YEARS} variables={{ year }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      return (
        // <div className="div-games-of-the-year">
        data.Game.map(game => (
          <Game key={game.title + game._id} game={game} fun={fun} />
        ))
        // </div>
      );
    }}
  </Query>
);

export default GamesOfTheYears;
