import React, { useState } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";

import { Router } from "@reach/router";
//import Search from "./Components/Search/Search.js";

import "./AllGamesStyle.css";
import Game from "../Game/Game.js";

const AllGames = props => {
  //const {loading: loadingN, data: allGames, refetch: refetchN} = useQuery(GET_ALL_GAMES);
  //if(loadingN) return null;

  const { platform } = props;
  const { allGames = [] } = props;
  const { funkcija } = props;

  //var niz = allGames.map(x => {return x});
  //var arr = allGames.Game.filter(game => game.price === platform);
  //console.log('Arr je: ' + arr);
  function fun(input) {
    funkcija(input);
  }

  return (
    <div className="div-games-all">
      {allGames.Game.map(game => (
        <Game
          key={game.title + game.id}
          game={game}
          fun={fun}
          btnVisible={"visible"}
        />
      ))}
    </div>
  );
};
export default AllGames;
