import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import { Router } from "@reach/router";

import Game from "../Game/Game.js";

// import { StoreContainer } from "./store";

const GET_GAMES_BY_PLATFORM = gql`
  query getGamesByPlatform($plat: String = "") {
    Game(filter: { platforms: { Platform: { name: $plat } } }) {
      _id
      title
      cover
      released
      price
      genre
      quantity
      image
      synopsis
    }
  }
`;

const PlatformFilter = ({ plat }, { funkcija }) => (
  <Query query={GET_GAMES_BY_PLATFORM} variables={{ plat }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      console.log(data.Game);
      return (
        <div className="div-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game._id}
              game={game}
              fun={funkcija}
              btnVisible={"visible"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default PlatformFilter;
