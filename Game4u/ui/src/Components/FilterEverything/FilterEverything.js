import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import { Router } from "@reach/router";

import Game from "../Game/Game.js";

// import { StoreContainer } from "./store";

const GET_GAMES_BY_SEARCH__AND_PLATFORM = gql`
  query getGamesByPlatform(
    $plat: String = ""
    $comp: [String!]
    $name: String = ""
  ) {
    Game(
      filter: {
        AND: [
          { title_contains: $name }
          { platforms: { Platform: { name_contains: $plat } } }
          { company: { Company: { name_in: $comp } } }
        ]
      }
    ) {
      _id
      title
      genre
      released
      price
      quantity
      image
      cover
      synopsis
    }
  }
`;

const FilterEverything = ({ plat, comp, name }) => (
  <Query
    query={GET_GAMES_BY_SEARCH__AND_PLATFORM}
    variables={{ plat, comp, name }}
  >
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      console.log("NAME JE: " + name);
      console.log("Comp je " + comp);
      return (
        <div className="div-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game._id}
              game={game}
              btnVisible={"visible"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default FilterEverything;
