import React, { useState, useEffect, useCallback } from 'react';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { Query } from "react-apollo";

import { Router } from '@reach/router';



import Game from '../Game/Game.js';


const SORT_BY_EVERYTHING = gql`
query SORT_BY_TITLE_DESC($order: [_GameOrdering]){
    Game(orderBy: $order){
        _id
        title
        genre
        price
        cover
        image
        released
        rating
        quantity
        synopsis
    }
}
`;

const SortBy = ({order}) => (

    <Query query={SORT_BY_EVERYTHING} variables={{order}} >
       {
           ({loading, error, data})=>{
               if (loading) return null;
               if (error) return `Error!: ${error}`;


               return (
                    <div className="div-games-all">
                        {data.Game.map(game => (
                            <Game key={game.title + game._id} game={game}/>
                        ))}
                    </div>
            	);
           }
       }
   </Query>
);

export default SortBy;