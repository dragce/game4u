import React, { useState } from 'react';
import gql from 'graphql-tag';
import "./FilterPlatform/FilterPlatform.js";

const FilterPlatforms = props => {

	const { filters } = props;
	const { fun } = props;

	function fun(platName){
		fun(platName);
	}

	return (
		<div className="div-platforms-all">
			{filters.Platform.map(plat => (
				<FilterPlatform key={plat.name + plat._id} filter={plat} fun={fun} />
			))}
		</div>
    );
};
export default FilterPlatforms;
