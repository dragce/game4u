import React from 'react';
import FilterByYear from './FilterByYear';


const FiltersByYear = (props) => {
    
    const {funkcija} = props;
    const {years} = props;
    console.log(props);

    const duplicateYears = years.Game.map(item => item.released);

    const uniqueYears = [...new Set(duplicateYears)];
    

    function filterByYear(input, click){
        funkcija(input, click);
    }
    
    return (
       <div className="filters-all">
           {
               uniqueYears.map(year => (<FilterByYear key={year} year={year} funkcija={filterByYear} />))
           }
       </div>
    )
}

export default FiltersByYear;