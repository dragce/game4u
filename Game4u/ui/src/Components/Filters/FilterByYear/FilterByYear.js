import React, { useState } from 'react';

const FilterByYear = (props) => {
    
    const [clicked, setClicked] = useState(false);
    const {funkcija} = props;
    const zero = "0";
    const {year} = props;
    const [btnActive, setBtnActive] = useState("btn-Filter");
    
    return (
       <div className="filter-company" key={year}>
           <button className={btnActive} 
           
           key={year}

           onClick={() => {
                if(clicked){
                    setClicked(false);
                    funkcija(year, false);
                    setBtnActive("btn-Filter");
                }
                else{
                    setClicked(true);
                    funkcija(year, true);
                    setBtnActive("btn-Filter-Active");
                }

			}}>
                {year}
			</button>
       </div>
    )
}

export default FilterByYear;