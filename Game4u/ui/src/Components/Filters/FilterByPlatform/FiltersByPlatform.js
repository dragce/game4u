import React from 'react';
import FilterByPlatform from './FilterByPlatform';


const FiltersByPlatforms = (props) => {
    
    const {funkcija} = props;
    const {platforms} = props;

    //const duplicatePlatforms = platforms.Platform.map(item => item.name);
    //const uniquePlatforms = [...new Set(duplicatePlatforms)];
    //console.log(platforms);

    function filterByPlatform(input, click){
        funkcija(input, click);
    }
    
    return (
       <div className="filters-all">
           {
               platforms.Platform.map(platform => (<FilterByPlatform key={platform.name + platform._id} platform={platform} funkcija={filterByPlatform} />))
           }
       </div>
    )
}

export default FiltersByPlatforms;