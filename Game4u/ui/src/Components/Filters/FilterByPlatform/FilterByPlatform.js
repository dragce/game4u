import React, { useState } from 'react';

const FilterByPlatform = (props) => {
    
    const [clicked, setClicked] = useState(false);
    const {funkcija} = props;
    const {platform} = props;
    const [btnActive, setBtnActive] = useState("btn-Filter");
    const none = "";
    
    
    return (
           <button className={btnActive} 
           
           key={platform.name + platform.id}

           onClick={() => {
                if(clicked){
                    setClicked(false);
                    funkcija(none, false);
                    setBtnActive("btn-Filter");
                }
                else{
                    setClicked(true);
                    funkcija(platform.name, true);
                    setBtnActive("btn-Filter-Active");
                    console.log("PLATFORM JE: " + platform);
                    console.log("PLATFORM JE: " + platform.name);
                }

			}}>
                {platform.name}
			</button>
    )
}

export default FilterByPlatform;