import React, { useState } from 'react';

const FilterByCompany = (props) => {
    
    const [clicked, setClicked] = useState(false);
    const {funkcija} = props;
    const none = "";
    const {company} = props;
    const [btnActive, setBtnActive] = useState("btn-Filter");
    
    return (
           <button className={btnActive} 
           
           key={company._id + company.name}

           onClick={() => {
                if(clicked){
                    setClicked(false);
                    funkcija(company.name, false);
                    setBtnActive("btn-Filter");
                }
                else{
                    setClicked(true);
                    funkcija(company.name, true);
                    setBtnActive("btn-Filter-Active");
                }

			}}>
                {company.name}
			</button>
    )
}

export default FilterByCompany;