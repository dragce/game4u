import React from 'react';
import FilterByCompany from './FilterByCompany';


const FiltersByCompany = (props) => {
    
    const {funkcija} = props;
    //const {input} = props;
    const {companies} = props;

    function filterByCompany(input, click){
        funkcija(input, click);
    }
    
    return (
       <div className="filters-all">
           {
               companies.Company.map(comp => (<FilterByCompany key={comp._id} company={comp} funkcija={filterByCompany} />))
           }
       </div>
    )
}

export default FiltersByCompany;