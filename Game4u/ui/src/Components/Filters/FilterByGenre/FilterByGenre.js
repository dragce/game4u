import React, { useState } from 'react';

const FilterByGenre = (props) => {
    
    const [clicked, setClicked] = useState(false);
    const {funkcija} = props;
    const {genre} = props;
    const [btnActive, setBtnActive] = useState("btn-Filter");
    const none = "";
    
    return (
       
           <button className={btnActive} 
           
           key={genre}

           onClick={() => {
                if(clicked){
                    setClicked(false);
                    funkcija(genre, false);
                    console.log(genre);
                    setBtnActive("btn-Filter");
                }
                else{
                    setClicked(true);
                    funkcija(genre, true);
                    setBtnActive("btn-Filter-Active");
                }

			}}>
                {genre}
			</button>
    )
}

export default FilterByGenre;