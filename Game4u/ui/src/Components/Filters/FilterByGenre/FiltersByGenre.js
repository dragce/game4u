import React from 'react';
import FilterByGenre from './FilterByGenre';


const FiltersByGenre= (props) => {
    
    const {funkcija} = props;
    const {genres} = props;

    const duplicateGenres = genres.Game.map(item => item.genre);
    const uniqueGenres = [...new Set(duplicateGenres)];


    function filterByGenre(input, click){
        funkcija(input, click);
    }
    
    return (
       <div className="filters-all">
           {
               uniqueGenres.map(genre => (<FilterByGenre key={genre} genre={genre} funkcija={filterByGenre} />))
           }
       </div>
    )
}

export default FiltersByGenre;