import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import { Router } from "@reach/router";

import sample from "./video.mp4";

import Game from "../Game/Game.js";
import "./Style.css";
import SimilarGames from "../SimilarGames/SimilarGames.js";
import AllGamesPerPlatform from "../AllPlatformsPerGame/AllGamesPerPlatform.js";

function GamePage(props) {
  const [loading, setLoading] = useState(false);
  const [opacity, setOpacity] = useState(false);

  const igra = props.location.state.game;
  //const { igra } = props.location.state.game;
  console.log(igra.title);
  console.log(igra.genre);
  window.scrollTo(0, 0);

  useEffect(() => {
    setTimeout(() => setLoading(true), 2500);
    setTimeout(() => setOpacity(true), 1200);
  }, []);

  let pozadina;
  igra.cover === undefined
    ? (pozadina = "#000000")
    : (pozadina = "url(" + igra.cover + ")");

  return (
    <div className="game-page">
      <div
        className="loader-div"
        style={
          !loading
            ? { visibility: "visible", opacity: "1" }
            : opacity
            ? {
                visibility: "hidden",
                opacity: "0",

                zIndex: "-100 "
              }
            : {
                visibility: "hidden",
                opacity: "0",

                zIndex: "-100 "
              }
        }
      >
        <div className="loader"></div>
      </div>
      <div
        className="game-page-1"
        style={{
          height: "100vh",
          width: "100vw",
          background: `url(${igra.cover})`,
          backgroundSize: "cover",
          backgroundPosition: "center"
        }}
      >
        <div className="header">
          <div>
            <h1 className="game4u-logo">{igra.title.toUpperCase()}</h1>
            <p>
              {igra.genre} : {igra.released}
            </p>
            <p>RATING : {igra.rating}</p>
          </div>
        </div>
        <div className="content">
          <div className="div-synopsis">
            <div className="synopsis-eve">
              <h1 className="synopsis">Synopsis</h1>
              <p>{igra.synopsis}</p>
            </div>
          </div>
          <div className="similar-games">
            <h1 className="synopsis">This Game is available on:</h1>
            <AllGamesPerPlatform title={igra.title} />
          </div>
        </div>
        <div className="footer"></div>
      </div>
      <div className="game-page-3">
        <div className="banner">
          <video id="background-video" loop autoPlay muted>
            <source src={sample} type="video/mp4" />
            <source src={sample} type="video/ogg" />
            Your browser does not support the video tag.
          </video>
        </div>
        <div className="cont-similar">
          <div className="content">
            <div className="similar-games">
              <h1 className="synopsis">You may also like:</h1>
              <SimilarGames title={igra.title} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default GamePage;
