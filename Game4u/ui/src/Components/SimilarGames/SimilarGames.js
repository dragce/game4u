import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import { Router } from "@reach/router";

import Game from "../Game/Game.js";

const SIMILAR_GAMES = gql`
  query SIMILAR($title: String = "") {
    Game(
      filter: {
        AND: [
          { title: $title }
          { platforms: { Platform: { name_contains: "" } } }
        ]
      }
    ) {
      similar_with {
        to {
          Game {
            platforms(filter: { Platform: { name: "PS4" } }) {
              Platform {
                name
              }
            }
            id
            title
            genre
            price
            cover
            image
            released
            rating
            quantity
          }
        }
      }
    }
  }
`;

const SimilarGames = ({ title }) => (
  <Query query={SIMILAR_GAMES} variables={{ title }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      console.log(
        "RESPONSE JE U SIMILAR GAME JE: " + data.Game[0].similar_with.to
      );

      let respo = data.Game[0].similar_with.to;
      console.log("RESPONSE GAME JE: " + respo);

      let pomArr = respo.filter(item => item.Game.platforms.length > 0);

      console.log("RESPONSE pomArr JE: " + pomArr);

      return (
        <div className="div-similar-games-all">
          {pomArr.map(game => (
            <Game
              key={game.Game.title + game.Game.id}
              game={game.Game}
              btnVisible={"hidden"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default SimilarGames;
