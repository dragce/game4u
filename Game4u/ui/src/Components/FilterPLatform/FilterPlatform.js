import React from 'react';
//import "./FilterStyle.css";

const FilterPlatforms = (props) => {

    const { filter } = props;
    const { fun } = props;


    return (
        <div className="div-filter">
            <button onClick={()=> fun(filter.name)}>{filter.name}</button>
        </div>
    )
}

export default FilterPlatforms;