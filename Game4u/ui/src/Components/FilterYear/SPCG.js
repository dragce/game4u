import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import { Router } from "@reach/router";

import Game from "../Game/Game.js";

// import { StoreContainer } from "./store";

const GET_GAMES_BY_SEARCH__AND_PLATFORM = gql`
  query getSearchAndFitlerGamesByPlatform(
    $plat: String = ""
    $name: String = ""
    $comp: String = ""
    $year: Int
    $genre: String = ""
    $order: [_GameOrdering]
  ) {
    Game(
      orderBy: $order
      filter: {
        AND: [
          { title_contains: $name }
          { platforms: { Platform: { name_contains: $plat } } }
          { company: { Company: { name_contains: $comp } } }
          { genre_contains: $genre }
          { released_not: $year }
        ]
      }
    ) {
      _id
      cover
      title
      genre
      released
      price
      quantity
      image
      synopsis
    }
  }
`;

const SPCG = ({ plat, name, comp, year, genre, order, fun }) => (
  <Query
    query={GET_GAMES_BY_SEARCH__AND_PLATFORM}
    variables={{ plat, name, comp, year, genre, order }}
  >
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      console.log("NAME JE: " + name);
      console.log("COMP JE: " + comp);
      console.log("GENRE JE: " + genre);

      return (
        <div className="div-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game.id}
              game={game}
              fun={fun}
              btnVisible={"visible"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default SPCG;
