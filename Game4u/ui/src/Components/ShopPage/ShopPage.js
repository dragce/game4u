import React, { useState, useEffect, useCallback } from "react";
//import gql from 'graphql-tag';
//import { useQuery } from '@apollo/react-hooks';
//import { Query } from "react-apollo";

//import { Router } from '@reach/router';

//import sample from "./video.mp4";

//import Game from '../Game/Game.js';
import "./ShopPage.css";
import SimilarGames from "../SimilarGames/SimilarGames.js";
import AllGamesPerPlatform from "../AllPlatformsPerGame/AllGamesPerPlatform.js";

function ShopPage(props) {
  const igrice = props.location.state.shoppingList;
  console.log(props);
  console.log(igrice);
  //const { igra } = props.location.state.game;
  window.scrollTo(0, 0);

  return (
    <div className="game-page-shop">
      <div
        className="game-page-1-shop"
        style={{
          height: "100%",
          width: "100%",
          backgroundSize: "cover",
          backgroundPosition: "center"
        }}
      >
        <div className="header">
          <div>
            <h1 className="game4u-logo">Shopping List</h1>
          </div>
        </div>
        <div className="content-shop">
          {igrice.map(item => (
            <div className="game-item-shop" key={item.id}>
              <div className="image-shop">
                <img className="image-image-shop" src={item.image}></img>
              </div>
              <div className="cont-shop">
                <div className="header-shop">
                  <h1>{item.title}</h1>
                </div>
                <div className="sub-header-shop">
                  <h3>
                    {item.genre} {item.released}
                  </h3>
                </div>
                <div className="synopsis-div-shop">
                  <h3>{item.synopsis}</h3>
                </div>
                <div className="price-shop">
                  <h3>PRICE: {item.price}$</h3>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
export default ShopPage;
