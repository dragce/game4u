import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import Game from "../Game/Game.js";

const COMP_GENRE = gql`
  query getSearchAndFitlerGamesByPlatform(
    $name: String = ""
    $comps: [String!]
    $genres: [String!]
    $plat: String = ""
    $order: [_GameOrdering]
  ) {
    Game(
      orderBy: $order
      filter: {
        AND: [
          { title_contains: $name }
          { company: { Company: { name_in: $comps } } }
          { platforms: { Platform: { name_contains: $plat } } }
          { genre_in: $genres }
        ]
      }
    ) {
      id
      title
      genre
      rating
      synopsis
      released
      price
      quantity
      image
      cover
    }
  }
`;

const CompGenre = ({ name, comps, genres, plat, order, fun }) => (
  <Query query={COMP_GENRE} variables={{ name, comps, genres, plat, order }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;
      //console.log("GODINE SU U MoreThanOne: " + years);

      return (
        <div className="div-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game.id}
              game={game}
              fun={fun}
              btnVisible={"visible"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default CompGenre;
