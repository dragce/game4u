import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import Game from "../Game/Game.js";

const COMP_YEAR = gql`
  query getSearchAndFitlerGamesByPlatform(
    $name: String = ""
    $comps: [String!]
    $years: [Int!]
    $plat: String = ""
    $order: [_GameOrdering]
  ) {
    Game(
      orderBy: $order
      filter: {
        AND: [
          { title_contains: $name }
          { platforms: { Platform: { name_contains: $plat } } }
          { company: { Company: { name_in: $comps } } }
          { released_in: $years }
        ]
      }
    ) {
      id
      title
      genre
      rating
      synopsis
      released
      price
      quantity
      image
      cover
    }
  }
`;

const CompYear = ({ name, comps, years, plat, order, fun }) => (
  <Query query={COMP_YEAR} variables={{ name, comps, years, plat, order }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      return (
        <div className="div-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game.id}
              game={game}
              fun={fun}
              btnVisible={"visible"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default CompYear;
