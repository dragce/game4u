import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import Game from "../Game/Game.js";

const GENRE_YEAR = gql`
  query getSearchAndFitlerGamesByPlatform(
    $name: String = ""
    $years: [Int!]
    $genres: [String!]
    $plat: String = ""
    $order: [_GameOrdering]
  ) {
    Game(
      orderBy: $order
      filter: {
        AND: [
          { title_contains: $name }
          { genre_in: $genres }
          { platforms: { Platform: { name_contains: $plat } } }
          { released_in: $years }
        ]
      }
    ) {
      id
      title
      genre
      rating
      synopsis
      released
      price
      quantity
      image
      cover
    }
  }
`;

const GenreYear = ({ name, years, genres, plat, order, fun }) => (
  <Query query={GENRE_YEAR} variables={{ name, years, genres, plat, order }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;
      console.log("GODINE SU U MoreThanOne: " + years);

      return (
        <div className="div-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game.id}
              game={game}
              fun={fun}
              btnVisible={"visible"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default GenreYear;
