import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { Query } from "react-apollo";

import Game from "../Game/Game.js";

const COMP = gql`
  query getCompanies(
    $name: String = ""
    $comps: [String!]
    $plat: String = ""
    $order: [_GameOrdering]
  ) {
    Game(
      orderBy: $order
      filter: {
        AND: [
          { title_contains: $name }
          { platforms: { Platform: { name_contains: $plat } } }
          { company: { Company: { name_in: $comps } } }
        ]
      }
    ) {
      id
      title
      genre
      rating
      synopsis
      released
      price
      quantity
      image
      cover
    }
  }
`;

const Companies = ({ fun, name, comps, plat, order }) => (
  <Query query={COMP} variables={{ name, comps, plat, order }}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;

      console.log("GAMES SU U COMPANIES: " + data.Game);

      return (
        // <div className="div-games-of-the-year">
        <div className="div-games-all">
          {data.Game.map(game => (
            <Game
              key={game.title + game.id}
              game={game}
              fun={fun}
              btnVisible={"visible"}
            />
          ))}
        </div>
      );
    }}
  </Query>
);

export default Companies;
