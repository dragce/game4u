import React from "react";
import "./GameStyle.css";
import { Link } from "react-router-dom";
import gql from "graphql-tag";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Mutation } from "react-apollo";

const UPDATE_QUANTITY = gql`
  mutation Update_Quantity($id: String!, $quant: Int!) {
    UpdateGame(id: $id, quantity: $quant) {
      title
      quantity
    }
  }
`;

const Game = props => {
  //const [updateQuantity] = useMutation(UPDATE_QUANTITY);
  const { game } = props;
  const id = game.id;
  let updateValue = game.quantity - 1;
  const { fun } = props;
  const { btnVisible } = props;

  let btnAv = "visible";

  return (
    <div className="div-game">
      <Link to={{ pathname: `/game/${game.id}`, state: { game } }}>
        {console.log("Game je: " + game)}
        <div className="game-image">
          <img src={game.image}></img>
        </div>
      </Link>

      <div className="game-info">
        <div
          className="game-title"
          style={{
            color: "white",
            fontFamily: "'Advent Pro', sans-serif",
            fontSize: "20px",
            padding: "0",
            margin: "0"
          }}
        >
          {game.title}
        </div>
        <div
          className="game-price"
          style={{
            height: "auto",
            width: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            color: "white",
            fontFamily: "'Advent Pro', sans-serif",
            padding: "0",
            marginTop: "20px"
          }}
        >
          <h1 style={{ fontSize: "25px" }}>PRICE: {game.price}$</h1>

          <Mutation mutation={UPDATE_QUANTITY}>
            {(updateQuantity, { data }) => (
              <button
                className="btn-buy"
                style={{ visibility: `${btnVisible}` }}
                onClick={e => {
                  e.preventDefault();
                  updateQuantity({ variables: { id: id, quant: updateValue } });
                  fun(game);
                }}
              >
                BUY
              </button>
            )}
          </Mutation>
        </div>
      </div>
    </div>
  );
};

export default Game;
