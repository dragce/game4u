import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import {BrowserRouter, Route} from "react-router-dom";
import GamePage from "./Components/GamePage/GamePage.js";
import ShopPage from "./Components/ShopPage/ShopPage.js";

const client = new ApolloClient({
	uri: 'http://localhost:4000',
	onError(all) {
		console.log(all);
	}
});

const Main = () => (
	<ApolloProvider client={client}>
		<BrowserRouter>
			<Route exact path="/">
				<App />
			</Route>
			<Route path="/game/:id" component={GamePage} />
			<Route path="/shoppingcart/" component={ShopPage} />
		</BrowserRouter>
	</ApolloProvider>
);

ReactDOM.render(<Main />, document.getElementById('root'));
