import React, { useState, useEffect, useCallback } from "react";
import gql from "graphql-tag";
import { useQuery, useMutation } from "@apollo/react-hooks";

//import { Router } from '@reach/router';
import { Router, Route, Link } from "react-router-dom";
import Search from "./Components/Search/Search.js";

import "./App.css";
import AllGames from "./Components/AllGames/AllGames.js";
import PlatformFilter from "./Components/PlatformFilter/PlatformFilter.js";
import SearchPlatformFilter from "./Components/SearchAndFilterByPlatform/SearchFilterPlatform.js";
import SearchPlatformCompanyFilter from "./Components/CompanyPlatformSearch/CompanyPlatformSearch.js";
import FilterPlatforms from "./Components/FilterPLatform/FilterPlatform.js";
import SPCYG from "./Components/FilterYear/SPCYG.js";
import SPCG from "./Components/FilterYear/SPCG.js";
import SortBy from "./Components/Sorting/SortBy.js";
import GamesOfTheYears from "./Components/GamesOfTheYears/GamesOfTheYears.js";
import GamePage from "./Components/GamePage/GamePage.js";
import FiltersByCompany from "./Components/Filters/FilterByCompany/FiltersByCompany.js";
import FiltersByYear from "./Components/Filters/FilterByYear/FiltersByYear.js";
import FiltersByGenre from "./Components/Filters/FilterByGenre/FiltersByGenre.js";
import FiltersByPlatforms from "./Components/Filters/FilterByPlatform/FiltersByPlatform.js";
import MoreFilters from "./Components/MoreFilters/MoreFilters.js";
import CompYear from "./Components/MoreFilters/CompYear.js";
import CompGenre from "./Components/MoreFilters/CompGenre.js";
import Companies from "./Components/MoreFilters/Companies.js";
import GenreYear from "./Components/MoreFilters/GenreYear.js";
import Genres from "./Components/MoreFilters/Genre.js";
import Years from "./Components/MoreFilters/Year.js";

const GET_PLATFORMS = gql`
  {
    Platform {
      _id
      name
    }
  }
`;

const GET_COMPANIES = gql`
  query AllCompanies {
    Company {
      _id
      name
    }
  }
`;

const GET_YEARS = gql`
  query AllYears {
    Game(orderBy: released_asc) {
      released
    }
  }
`;

const GET_GENRES = gql`
  query AllGenres {
    Game {
      genre
    }
  }
`;

const GET_GAME = gql`
  query SearchForGame($searchTerm: String = "") {
    Game(filter: { title_contains: $searchTerm }) {
      id
      title
      genre
      rating
      synopsis
      released
      price
      quantity
      image
      cover
    }
  }
`;

const GET_NEWEST_GAME = gql`
  query NewestGames {
    Game(
      orderBy: released_desc
      first: 4
      filter: { platforms: { Platform: { name: "PS4" } } }
    ) {
      id
      title
      genre
      rating
      synopsis
      released
      price
      quantity
      image
      cover
    }
  }
`;

const GET_ALL_GAMES = gql`
  {
    Game {
      id
      title
      genre
      rating
      synopsis
      released
      price
      quantity
      image
      cover
    }
  }
`;

const App = () => {
  //#region BASIC STATES
  const [searchTerm, setSearchTerm] = useState("");
  const [filterTerm, setFilterTerm] = useState("");
  const [clickedPlatform, setClickedPlatform] = useState("");
  const [globalTarget, setPGlobalTarget] = useState("");
  const [filterByPlatform, setFilterByPlatform] = useState(false);

  const [triger, setTriger] = useState(false);

  const [clickedCompany, setClickedCompany] = useState("");
  const [filterByCompany, setFilterByCompany] = useState(false);
  const [filterByCompanies, setFilterByCompanies] = useState([]);

  const [filterByYear, setFilterByYear] = useState(false);
  const [clickedYear, setClickedYear] = useState("");
  const [filterByYears, setFilterByYears] = useState([]);

  const [filterByGenre, setFilterByGenre] = useState(false);
  const [clickedGenre, setClickedGenre] = useState("");
  const [filterByGenres, setFilterByGenres] = useState([]);

  const [sortClicked, setSortClicked] = useState(false);
  const [sortByOrder, setSortOrder] = useState("_id_asc");

  const [hasResults, setHasResults] = useState(false);
  const [hasFilters, setHasFilters] = useState(false);
  const [filtersArray, setFiltersArray] = useState([]);

  const [widthFilt, setWidthFilt] = useState(0);
  const [header, setHeader] = useState("btn-header-close");

  const [displayYN1, setDisplayYN1] = useState("main-div-one");
  const [displayYN2, setDisplayYN2] = useState("main-div-two");
  const [displayYN4, setDisplayYN4] = useState("main-div-four");
  const [appHeightZero, setAppHeightZero] = useState("App");

  const [clicked, setClicked] = useState(0);
  const [shoppingCart, setShoppingCart] = useState(0);
  let oneGame = {};
  const [shoppingList, setShoppingList] = useState([]);

  const { loading: loading2, data: allCompanies, refetch: refetch2 } = useQuery(
    GET_COMPANIES
  );
  const { loading: loading3, data: allGames, refetch: refetch3 } = useQuery(
    GET_ALL_GAMES
  );
  const { loading: loading4, data: gamesByTerm, refetch: refetch4 } = useQuery(
    GET_GAME
  );
  const { loading: loading5, data: newGames, refetch: refetch5 } = useQuery(
    GET_NEWEST_GAME
  );
  const { loading: loading6, data: allGenres, refetch: refetch6 } = useQuery(
    GET_GENRES
  );
  const { loading: loading7, data: allYears, refetch: refetch7 } = useQuery(
    GET_YEARS
  );
  const {
    loading: loading10,
    data: allPlatforms,
    refetch: refetch10
  } = useQuery(GET_PLATFORMS);
  //#endregion

  //const uniqueGenres = [...new Set(allGenres.Game)];

  function FilterByPlatform(input, click) {
    setClickedPlatform(input);
    setFilterByPlatform(click);
    console.log("FILTER BY PLATFORM " + click);
  }

  function FilterByYear(input, click) {
    !click ? setClickedYear("0") : setClickedYear(input);
    setFilterByYear(click);
    console.log(clickedYear);

    click
      ? setFilterByYears([...filterByYears, input])
      : setFilterByYears([...filterByYears.filter(i => i !== input)]);
  }

  function FilterByCompany(input, click) {
    !click ? setClickedCompany("") : setClickedCompany(input);
    setFilterByCompany(click);
    console.log(clickedCompany);

    click
      ? setFilterByCompanies([...filterByCompanies, input])
      : setFilterByCompanies([...filterByCompanies.filter(i => i !== input)]);
  }

  function FilterByGenre(input, click) {
    !click ? setClickedGenre("") : setClickedGenre(input);
    setFilterByGenre(click);

    click
      ? setFilterByGenres([...filterByGenres, input])
      : setFilterByGenres([...filterByGenres.filter(i => i !== input)]);
  }

  function handleChange({ target }) {
    target.value.length == 0 ? setHasResults(false) : setHasResults(true);
    setSearchTerm(target.value);

    refetch2({});
    refetch3({});
    refetch4({ searchTerm });
    refetch5({});
  }

  const onChange = e => {
    setSearchTerm(e.target.value);
    handleChange(e);
    console.log(e.target.value);
    console.log(gamesByTerm);
  };

  function FilterPlatform(plat) {
    setFilterByPlatform(true);
    setClickedPlatform(plat);
  }

  function BuyGame(input) {
    setShoppingCart(shoppingCart + 1);
    setShoppingList([...shoppingList, input]);
    console.log("U Shopping Listu je dodato: " + input);
    console.log("Shopping List: " + shoppingList);
  }
  //provera

  if (loading2 || loading3 || loading5 || loading10) return null;
  console.log("All Games " + allGames);
  //	console.log('Platforms ' + platforms);
  //console.log("Plat Games " + platGames);
  //setGameArr(allGames);
  //window.scrollTo(200, 200);

  return (
    // <Router>
    // <Route path="/game" component={GamePage}/>

    <div className="App">
      <div className={displayYN1}>
        <div className="header-div">
          <h1 className="game4u-logo">GAME:4U</h1>
          <ul className="game4u-nav">
            <li>all games</li>
            <li>newest</li>
            <li>best selling</li>
            <li>our choice</li>
          </ul>
          <div className="header-search">
            <input
              type="text"
              placeholder="Search.."
              className="inputBox"
              placeholder="search..."
              onChange={onChange}
            />
          </div>
          <Link to={{ pathname: `/shoppingcart/`, state: { shoppingList } }}>
            <div className="spann">
              <div
                className="span"
                style={{ zIndex: "2", position: "absolute" }}
              >
                <span id="val" value={shoppingCart}>
                  {shoppingCart}
                </span>
              </div>
              <div style={{ height: "40px", width: "40px" }}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  height="40px"
                  viewBox="0 -31 512.00033 512"
                  width="40px"
                  className="svg"
                >
                  <g>
                    <path
                      d="m166 300.003906h271.003906c6.710938 0 12.597656-4.4375 14.414063-10.882812l60.003906-210.003906c1.289063-4.527344.40625-9.390626-2.433594-13.152344-2.84375-3.75-7.265625-5.964844-11.984375-5.964844h-365.632812l-10.722656-48.25c-1.523438-6.871094-7.617188-11.75-14.648438-11.75h-91c-8.289062 0-15 6.710938-15 15 0 8.292969 6.710938 15 15 15h78.960938l54.167968 243.75c-15.9375 6.929688-27.128906 22.792969-27.128906 41.253906 0 24.8125 20.1875 45 45 45h271.003906c8.292969 0 15-6.707031 15-15 0-8.289062-6.707031-15-15-15h-271.003906c-8.261719 0-15-6.722656-15-15s6.738281-15 15-15zm0 0"
                      data-original="#000000"
                      className="active-path"
                      data-old_color="#000000"
                      fill="#00ffd5"
                    />
                    <path
                      d="m151 405.003906c0 24.816406 20.1875 45 45.003906 45 24.8125 0 45-20.183594 45-45 0-24.8125-20.1875-45-45-45-24.816406 0-45.003906 20.1875-45.003906 45zm0 0"
                      data-original="#000000"
                      className="active-path"
                      data-old_color="#000000"
                      fill="#00ffd5"
                    />
                    <path
                      d="m362.003906 405.003906c0 24.816406 20.1875 45 45 45 24.816406 0 45-20.183594 45-45 0-24.8125-20.183594-45-45-45-24.8125 0-45 20.1875-45 45zm0 0"
                      data-original="#000000"
                      className="active-path"
                      data-old_color="#000000"
                      fill="#00ffd5"
                    />
                  </g>{" "}
                </svg>
              </div>
            </div>
          </Link>
        </div>
        <div className="main-div-one-content">
          <h3
            style={{
              fontSize: "70px",
              fontFamily: "'Teko', sans-serif",
              padding: "0",
              margin: "0",
              color: "white"
            }}
          >
            J.J. REDIS PRESENTS
          </h3>
          <h1
            style={{
              fontSize: "210px",
              fontFamily: "'Teko', sans-serif",
              padding: "0",
              margin: "0",
              color: "white"
            }}
          >
            GAME4U
          </h1>
        </div>
        <div className="main-one-footer">
          <div
            className="date"
            style={{
              color: "white",
              fontFamily: "'Advent Pro', sans-serif",
              fontSize: "30px"
            }}
          >
            <p>02. January 2020.</p>
          </div>
          <div
            style={{
              color: "white",
              fontFamily: "'Advent Pro', sans-serif",
              fontSize: "30px"
            }}
          >
            <p>J.J. REDIS</p>
          </div>
        </div>
      </div>

      <div className={displayYN2}>
        <div className="header-div-two" />
        <div className="filter-two">
          <h1
            style={{
              color: "white",
              fontFamily: "'Advent Pro', sans-serif",
              fontSize: "70px"
            }}
          >
            :LATEST GAMES
          </h1>
        </div>
        <AllGames allGames={newGames} platform={clicked} funkcija={BuyGame} />
        {console.log("NEW GAMES SU: " + newGames.Game)}
      </div>

      <div className="main-div-three">
        <div
          className="side-nav-bar-filter"
          style={{ width: `${widthFilt}vw` }}
        >
          <div className={header}>
            <button
              className="btn-Close"
              onClick={() => {
                setWidthFilt(0);
                setHeader("btn-header-close");
                setDisplayYN1("main-div-one");
                setDisplayYN2("main-div-two");
                setDisplayYN4("main-div-four");
                setAppHeightZero("App");
                //window.scrollTo(0,3000);
              }}
            >
              X
            </button>
            <p style={{ fontSize: "35px" }}>FILTERS and SORTING</p>
          </div>

          <div className="div-filter">
            <FiltersByCompany
              companies={allCompanies}
              funkcija={FilterByCompany}
            />
            <FiltersByYear years={allYears} funkcija={FilterByYear} />
            <FiltersByGenre genres={allGenres} funkcija={FilterByGenre} />
            <FiltersByPlatforms
              platforms={allPlatforms}
              funkcija={FilterByPlatform}
            />
            <button
              className="btn-Filter"
              value="PS4"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("title_asc");
              }}
            >
              TITLE ASC
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("title_desc");
              }}
            >
              TITLE DESC
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("rating_asc");
              }}
            >
              RAITING ASC
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("rating_desc");
              }}
            >
              RAITING DESC
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("price_asc");
              }}
            >
              PRICE ASC
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("price_desc");
              }}
            >
              PRICE DESC
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("released_asc");
              }}
            >
              YEAR ASC
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("released_desc");
              }}
            >
              YEAR DESC
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("quantity_asc");
              }}
            >
              BEST SELLING
            </button>
            <button
              className="btn-Filter"
              onClick={() => {
                setSortClicked(true);
                setSortOrder("quantity_desc");
              }}
            >
              MOST IN STOCK
            </button>
            <button
              className="btn-Filter"
              value="PS4"
              onClick={() => {
                setFilterByCompany(false);
                setFilterByPlatform(false);
                setClickedCompany("");
                setClickedPlatform("");

                setFilterByYear(false);
                setClickedYear();

                setFilterByGenre(false);
                setClickedGenre("");

                setSortClicked(false);
                setSortOrder("_id_asc");
              }}
            >
              RESET FILTERS
            </button>
          </div>
        </div>
        <div className="header-div-two" />
        <div className="filter-two">
          <h1
            style={{
              color: "white",
              fontFamily: "'Advent Pro', sans-serif",
              fontSize: "70px"
            }}
          >
            :GAMES
          </h1>
          <button
            className="btn-Close"
            onClick={() => {
              setWidthFilt(20);
              setHeader("btn-header");
              //	setDisplayYN1('main-div-one-close');
              //	setDisplayYN2('main-div-two-close');
              //	setDisplayYN4('main-div-four-close');
              //setAppHeightZero('App-Close');
            }}
          >
            +filters
          </button>
        </div>
        <div
          style={{
            height: "80vh",
            width: "100vw",
            alignItems: "center",
            display: "flex",
            justifyContent: "center"
          }}
        >
          {filterByCompanies.length > 0 ? (
            filterByYears.length > 0 ? (
              filterByGenres.length > 0 ? (
                <MoreFilters
                  name={searchTerm}
                  comps={filterByCompanies}
                  years={filterByYears}
                  genres={filterByGenres}
                  plat={clickedPlatform}
                  order={sortByOrder}
                  fun={BuyGame}
                />
              ) : (
                <CompYear
                  name={searchTerm}
                  comps={filterByCompanies}
                  years={filterByYears}
                  plat={clickedPlatform}
                  order={sortByOrder}
                  fun={BuyGame}
                />
              )
            ) : filterByGenres.length > 0 ? (
              <CompGenre
                name={searchTerm}
                comps={filterByCompanies}
                genres={filterByGenres}
                plat={clickedPlatform}
                order={sortByOrder}
                fun={BuyGame}
              />
            ) : (
              <Companies
                name={searchTerm}
                comps={filterByCompanies}
                plat={clickedPlatform}
                order={sortByOrder}
                fun={BuyGame}
              />
            )
          ) : filterByGenres.length > 0 ? (
            filterByYears.length > 0 ? (
              <GenreYear
                name={searchTerm}
                years={filterByYears}
                genres={filterByGenres}
                plat={clickedPlatform}
                order={sortByOrder}
                fun={BuyGame}
              />
            ) : (
              <Genres
                name={searchTerm}
                genres={filterByGenres}
                plat={clickedPlatform}
                order={sortByOrder}
                fun={BuyGame}
              />
            )
          ) : filterByYears.length > 0 ? (
            <Years
              name={searchTerm}
              years={filterByYears}
              plat={clickedPlatform}
              order={sortByOrder}
              fun={BuyGame}
            />
          ) : !filterByPlatform ? (
            !sortClicked ? (
              <AllGames
                platform={clickedPlatform}
                allGames={hasResults ? gamesByTerm : allGames}
                funkcija={BuyGame}
              />
            ) : (
              <SPCG
                plat={""}
                name={searchTerm}
                comp={""}
                year={0}
                genre={""}
                order={sortByOrder}
                fun={BuyGame}
              />
            )
          ) : !sortClicked ? (
            <SPCG
              plat={clickedPlatform}
              name={searchTerm}
              comp={""}
              year={0}
              genre={""}
              order={sortByOrder}
              fun={BuyGame}
            />
          ) : (
            <SPCG
              plat={clickedPlatform}
              name={searchTerm}
              comp={""}
              year={0}
              genre={""}
              order={sortByOrder}
              fun={BuyGame}
            />
          )}

          {/*  */}
        </div>
        }
      </div>

      <div className={displayYN4}>
        <div className="header-div-two" />
        <div className="filter-two">
          <h1
            style={{
              color: "white",
              fontFamily: "'Advent Pro', sans-serif",
              fontSize: "70px"
            }}
          >
            :BEST GAMES OF THE LAST 4 YEARS
          </h1>
          <div className="div-filter" />
        </div>
        <div
          style={{
            height: "75vh",
            width: "100vw",
            alignItems: "center",
            display: "flex",
            justifyContent: "space-around",
            flexWrap: "wrap",
            overflow: "auto"
          }}
        >
          {/* <MoreFilters ></MoreFilters> */}

          <GamesOfTheYears year={2019} fun={BuyGame} />

          <GamesOfTheYears year={2018} fun={BuyGame} />

          <GamesOfTheYears year={2017} fun={BuyGame} />

          <GamesOfTheYears year={2016} fun={BuyGame} />
        </div>
        }
      </div>
    </div>
    // </Router>
  );
};

export default App;

//{//	 (filterByPlatform || filterByCompany) ? <SearchPlatformCompanyFilter plat={clickedPlatform} name={searchTerm} comp={clickedCompany} /> : (<AllGames allGames={hasResults ? gamesByTerm : allGames} platform={clicked} funkcija={increaseCart}/>)
//(filterByPlatform || filterByCompany || filterByGenre || filterByYear) ? <SPCYG plat={clickedPlatform} name={searchTerm} comp={clickedCompany} year={parseInt(clickedYear)} genre={clickedGenre} /> : (<AllGames allGames={hasResults ? gamesByTerm : allGames} platform={clicked} funkcija={increaseCart}/>)
// filterByPlatform ||
// filterByCompany ||
// filterByGenre ||
// filterByYear ||
// sortClicked ? filterByYear ? (
// 	<SPCYG
// 		plat={clickedPlatform}
// 		name={searchTerm}
// 		comp={clickedCompany}
// 		year={parseInt(clickedYear)}
// 		genre={clickedGenre}
// 		order={sortByOrder}
// 	/>
// ) : (
// 	<SPCG
// 		plat={clickedPlatform}
// 		name={searchTerm}
// 		comp={clickedCompany}
// 		year={0}
// 		genre={clickedGenre}
// 		order={sortByOrder}
// 	/>
// ) : (
// 	<AllGames
// 		allGames={hasResults ? gamesByTerm : allGames}
// 		platform={clicked}
// 		funkcija={BuyGame}
// 	/>

// )}
