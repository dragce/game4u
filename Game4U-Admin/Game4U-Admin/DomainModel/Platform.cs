﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game4U_Admin.DomainModel
{
    public class Platform
    {
        public String name { get; set; }

        public Platform()
        {
           
        }

        public Platform(String n)
        {
            name = n;
        }
    }
}
