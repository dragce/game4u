﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game4U_Admin.DomainModel
{
    public class Game
    {
        public int id { get; set; }
        public String image { get; set; }
        public int quantity { get; set; }
        public String title { get; set; }
        public double price { get; set; }
        public int released { get; set; }
        public String genre { get; set; }
        public float rating { get; set; }
        public String cover { get; set; }
        public String synopsis { get; set; }
        public String developedByCompany { get; set; }
        /*  public List<Platform> availablePlatforms { get; set; }
          public List<Game> similarGames { get; set; }*/

        public String availablePlatform { get; set; }
        public List<String> similarGames { get; set; }

        public Game()
        {
            similarGames = new List<String>();
        }

     /*   public void addPlatform(String name)
        {
            availablePlatforms.Add(name);
        }*/

        public void addSimilarGame(String name)
        {
            similarGames.Add(name);
        }
    }
}

