﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4jClient;
using Neo4jClient.Cypher;
using Game4U_Admin.DomainModel;

namespace Game4U_Admin
{
    public partial class StartForm : Form
    {

        private GraphClient client;
     
        public StartForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "luka");
            try
            {
                client.Connect();
                
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);

          }
        }

        #region add
        private void companyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm af = new AddForm();
            af.header = "company";
            af.client = client;
            af.Show();
        }

        private void gameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm af = new AddForm();
            af.header = "game";
            af.client = client;
            af.Show();
        }

        private void platformToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm af = new AddForm();
            af.header = "platform";
            af.client = client;
            af.Show();
        }
        #endregion

        #region delete
        private void companyToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "companies";
            cf.client = client;
            cf.delete = true;
            cf.change = false;
            cf.Show();
        }

        private void platformToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "platforms";
            cf.client = client;
            cf.delete = true;
            cf.change = false;
            cf.Show();

        }
        
        private void gameToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "games";
            cf.client = client;
            cf.delete = true;
            cf.change = false;
            cf.Show();
        }
        #endregion

        #region change
        private void companyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "companies";
            cf.client = client;
            cf.Show();
        }

        private void gameToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "games";
            cf.client = client;
            cf.Show();
        }

        private void platformToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "platforms";
            cf.client = client;
            cf.Show();
        }

        #endregion

        #region search
        private void companyToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "companies";
            cf.change = false;
            cf.client = client;
            cf.Show(); 
        }

        private void gameToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "games";
            cf.client = client;
            cf.change = false;
            cf.Show();
        }

        private void platformToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            GridForm cf = new GridForm();
            cf.header = "platforms";
            cf.client = client;
            cf.change = false;
            cf.Show();
        }
        #endregion
    }
}
