﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game4U_Admin.DomainModel;
using Neo4jClient.Cypher;
using Neo4jClient;

namespace Game4U_Admin.DomainModel
{
    public partial class ChangeForm : Form
    {
        public Object item;
        public GraphClient client;
        public String header;
        public GridForm parentForm;
        private List<String> previouslySimilar = new List<String>(); 
        //za igrice sa kojima je selektovana do sada bila slicna

        public ChangeForm()
        {
            InitializeComponent();
        }

        private void ChangeForm_Load(object sender, EventArgs e)
        {
            label1.Text = "Change "+header;
            if (header != "game") //ako nije izabrano iz menija add game
            {
                lblQuantity.Visible = false;
                lblImage.Visible = false;
                lblPrice.Visible = false;
                lblCover.Visible = false;
                lblRating.Visible = false;
                lblGenre.Visible = false;
                lblSynopsis.Visible = false;
                label6.Visible = false;
                textBox2.Visible = false;
                textBox3.Visible = false;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                textBox7.Visible = false;
                richTextBox1.Visible = false;
                checkedListBox2.Visible = false;
                this.Height = 180;
                this.Width = 430;
                if (header == "company")
                {
                    Company c = (Company)item;
                    textBox1.Text = c.name;
                }
                else //platform
                {
                    Platform p = (Platform)item;
                    textBox1.Text = p.name;
                }

            }
            else //game
            {
                Game g = (Game)item;
                textBox1.Text = g.title;
                textBox2.Text = g.quantity.ToString();
                textBox3.Text = g.image;
                textBox4.Text = g.price.ToString();
                textBox5.Text = g.cover;
                textBox6.Text = g.rating.ToString();
                textBox7.Text = g.genre;
                richTextBox1.Text = g.synopsis;
                checkedListBox2.CheckOnClick = true;
                fillSimilarGames();
            }

        }
        private void fillSimilarGames()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Game) return distinct n.title",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<String> allGames = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList();
            
            var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:Game)-[:SIMILAR_WITH]->(g:Game) where n.title =~ \"" + ((Game)item).title + "\" return g.title",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            previouslySimilar = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query1).ToList();
            int i = 0;
            foreach (String s in allGames)
            {
                checkedListBox2.Items.Insert(i, s);
                foreach (String s1 in previouslySimilar)
                {
                    if (s == s1)
                    {
                        checkedListBox2.SetItemChecked(i, true);
                    }
                }
                i++;
            }
        }
        private void changeCompany()
        {
            
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Company) and n.name = \"" + ((Company)item).name + "\" set n.name = '" + textBox1.Text + "' return n",
                                                                        new Dictionary<string, object>(), CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
        }

        private void changeGame()
        {
            Game g = (Game)item;
            for(int i=0; i<checkedListBox2.CheckedItems.Count; i++)
            {
                g.addSimilarGame(checkedListBox2.CheckedItems[i].ToString());
                MessageBox.Show( checkedListBox2.CheckedItems[i].ToString());
            }
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("gname", textBox1.Text);
            queryDict.Add("gquant", Int32.Parse(textBox2.Text));
            queryDict.Add("gimage", textBox3.Text);
            queryDict.Add("gprice", Int32.Parse(textBox4.Text));
            queryDict.Add("gcover", textBox5.Text);
            queryDict.Add("grating", Int32.Parse(textBox6.Text));
            queryDict.Add("ggenre", textBox7.Text);
            queryDict.Add("gsyn", richTextBox1.Text);
            queryDict.Add("gid", ((Game)item).id);

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Game) where n.id ={gid} set n.title = {gname}, n.quantity={gquant}, n.image={gimage}, n.price={gprice}, n.cover={gcover}, n.rating={grating}, n.genre={ggenre}, n.synopsis={gsyn}",
                                                            queryDict, CypherResultMode.Set);
           ((IRawGraphClient)client).ExecuteCypher(query);
            for(int z=0; z<previouslySimilar.Count; z++)
            {
                String s = previouslySimilar[z]; 
                bool found = false;
                for(int c=0; c< g.similarGames.Count; c++)
                {
                    if (g.similarGames[c] == s) found = true;
                }
                if(found==false) //ako treba da se obrise veza
                {
                    var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Game)-[r:SIMILAR_WITH]->(p:Game) WHERE p.title='"+s+"' DELETE r",

                                                          queryDict, CypherResultMode.Set);
                    ((IRawGraphClient)client).ExecuteCypher(query3);
                }
            }

            foreach(String s2 in g.similarGames) {
                var query4 = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Game{ title: '" + textBox1.Text+ "'}), (p:Game { title: '"+s2+"' }) CREATE(n)-[:SIMILAR_WITH]->(p)",
                                                               queryDict, CypherResultMode.Set);
                ((IRawGraphClient)client).ExecuteCypher(query4);
            }
            
        }

        private void changePlatform()
        {
           
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Platform) and n.name = \"" + ((Platform)item).name + "\" set n.name = '" + textBox1.Text + "' return n",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (header == "company")
            {
                if (CheckCPinput())
                    changeCompany();
                else
                    MessageBox.Show("Morate popuniti sva polja");
            }
            else if (header == "game")
            {
                if (checkGameInput())
                    changeGame();
                else
                    MessageBox.Show("Morate popuniti sva polja");
            }
            else
            {
                if (CheckCPinput())
                    changePlatform();
                else
                    MessageBox.Show("Morate popuniti sva polja");
            }

            this.Close();
            parentForm.refreshTable();

        }

        private bool checkGameInput()
        {

            if (textBox1.Text == "")
                return false;

            if (textBox2.Text == "")
                return false;

            if (textBox3.Text == "")
                return false;

            if (textBox4.Text == "")
                return false;

            if (textBox5.Text == "")
                return false;

            if (textBox6.Text == "")
                return false;

            if (textBox7.Text == "")
                return false;

            if (richTextBox1.Text == "")
                return false;

            return true;
        }

        private bool CheckCPinput()
        {
            if (textBox1.Text == "")
                return false;
            else
                return true;
        }
    }
}
