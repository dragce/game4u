﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game4U_Admin.DomainModel;
using Neo4jClient.Cypher;
using Neo4jClient;

namespace Game4U_Admin
{
    public partial class GridForm : Form
    {
        public GraphClient client;
        public String header;
        public bool change=true;
        public bool delete = false;
        List<Company> companies=new List<Company>();
        List<Game> games=new List<Game>();
        List<Platform> platforms=new List<Platform>();

        public GridForm()
        {
            InitializeComponent();
        }

        private void ChangeForm_Load(object sender, EventArgs e)
        {
            if (!change) ChangeBtn.Visible = false;
            if (!delete) DeleteButton.Visible = false;
            if (header != "games") moreBtn.Visible = false;

            label1.Text =  header;
            refreshTable();

        }

        public void refreshTable()
        {
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();

            if (header == "companies")
            {
                showCompanies();
            }
            else if (header == "games")
            {
                showGames();
            }
            else if (header == "platforms")
            {
                showPlatforms();
            }
            else
            {
                MessageBox.Show("ERROR");
            }
        }
        private void showCompanies()
        {
            
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Company) RETURN n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            companies = ((IRawGraphClient)client).ExecuteGetCypherResults<Company>(query).ToList();


            dataGridView1.Columns.Add("name", "Company name");

            dataGridView1.Columns[0].Width = 300;
            dataGridView1.Width = 350;
            dataGridView1.BackgroundColor = Color.White;
            this.Width = 480;
            foreach (Company u in companies)
            {
                dataGridView1.Rows.Add(u.name);
            }
            
        }

        private void showGames()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Game) RETURN n ORDER BY n.id",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

             games = ((IRawGraphClient)client).ExecuteGetCypherResults<Game>(query).ToList();
            foreach(Game g in games)
            {
                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("id", g.id);

                var query1 = client.Cypher.Match("(g: Game { id: " + g.id + " })-[:AVAILABLE_ON]->(p: Platform)").Return<String>("p.name");
                String res = query1.Results.ToList<String>().FirstOrDefault<String>();
                g.availablePlatform = res;
               
            }

            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.Columns.Add("id", "ID");
            dataGridView1.Columns.Add("title", "Title");
            dataGridView1.Columns.Add("quantity", "Quantity");
            dataGridView1.Columns.Add("image", "Image");
            dataGridView1.Columns.Add("price", "Price");
            dataGridView1.Columns.Add("released", "Released");
            dataGridView1.Columns.Add("genre", "Genre");
            dataGridView1.Columns.Add("rating", "Rating");
            dataGridView1.Columns.Add("cover", "Cover");

            dataGridView1.Columns[0].Width = 40;
            dataGridView1.Columns[1].Width = 200;
            dataGridView1.Columns[3].Width = 200;
            dataGridView1.Columns[8].Width = 200;

            int width=0;
            for(int i=0; i < dataGridView1.Columns.Count; i++)
            {
                width += dataGridView1.Columns[i].Width;
            }
            
            dataGridView1.Width = width+50;
            this.Width = width + 150;
            
            foreach (Game u in games)
            {
                String[] row = { u.id.ToString(), u.title+" - "+u.availablePlatform, u.quantity.ToString(), u.image, u.price.ToString(), u.released.ToString(), u.genre, u.rating.ToString(), u.cover};
                dataGridView1.Rows.Add(row);
            }
           
        }

        private void showPlatforms()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Platform) RETURN n",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);

            platforms = ((IRawGraphClient)client).ExecuteGetCypherResults<Platform>(query).ToList();
           
            dataGridView1.Columns.Add("name", "Platform name");

            dataGridView1.Columns[0].Width = 300;
            dataGridView1.Width = 350;
            dataGridView1.BackgroundColor = Color.White;
            this.Width = 480;
            foreach (Platform u in platforms)
            {
                dataGridView1.Rows.Add(u.name);
            }
        }

        private void ChangeBtn_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 1)
            {
                MessageBox.Show("Morate selektovati samo jedan red!");
            }
            else if(dataGridView1.SelectedRows.Count == 1)
            {
                switch (header) { 
                
                    case "companies":
                        Company c = companies[dataGridView1.SelectedRows[0].Index];
                        ChangeForm cf2 = new ChangeForm();
                        cf2.client = client;
                        cf2.header = "company";
                        cf2.item = c;
                        cf2.parentForm = this;
                        cf2.Show();
                        break;
                    case "games":
                        Game g = games[dataGridView1.SelectedRows[0].Index];
                        ChangeForm cf1 = new ChangeForm();
                        cf1.client = client;
                        cf1.header = "game";
                        cf1.item = g;
                        cf1.parentForm = this;
                        cf1.Show();
                        break;
                    default:
                        Platform p =platforms[dataGridView1.SelectedRows[0].Index];
                        ChangeForm changeF = new ChangeForm();
                        changeF.client = client;
                        changeF.header = "platform";
                        changeF.item = p;
                        changeF.parentForm = this;
                        changeF.Show();
                        break;

                 }
                refreshTable();
            }
            else {
                MessageBox.Show("Kliknite na prvu kolonu reda za koji zelite da se prikaze");
            }
        }
        

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 1)
            {
                MessageBox.Show("Morate selektovati samo jedan red!");
            }
            else if (dataGridView1.SelectedRows.Count == 1)
            { 
            switch (header) {
                case "companies":
                    deleteCompany();
                    break;
                case "games":
                    deleteGame();
                    break;
                default:
                    deletePlatform();
                    break;
                }
                refreshTable();
            }
            else
            {
                MessageBox.Show("Kliknite na prvu kolonu reda za koji zelite da se prikaze");
            }
        }

        private void deleteCompany()
        {
            string companyName =  dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            Company company = new Company();
            company.name = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("companyName", companyName);
            var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:Company)-[:DEVELOPED]->(g:Game) where n.name = {companyName} detach delete g",
                                                            queryDict, CypherResultMode.Projection);
            ((IRawGraphClient)client).ExecuteCypher(query1);
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Company) where n.name = {companyName} detach delete n",
                                                            queryDict, CypherResultMode.Projection);

            ((IRawGraphClient)client).ExecuteCypher(query);
            
            MessageBox.Show("Deleted company " + company.name);
        }

        private void deleteGame()
        {
            string gameName = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string gameID = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int game_id = Int32.Parse(gameID);
            
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("gid", game_id); 

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Game) where n.id = {gid} detach delete n",
                                                            queryDict, CypherResultMode.Projection);

            ((IRawGraphClient)client).ExecuteCypher(query);

            MessageBox.Show("Deleted game " + gameName);
        }

        private void deletePlatform()
        {
            string platformName = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            Platform platform = new Platform();
            platform.name = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("platformName", platformName);
            var query1 = new Neo4jClient.Cypher.CypherQuery("match (g:Game)-[:AVAILABLE_ON]->(n:Platform) where n.name = {platformName} detach delete g",
                                                            queryDict, CypherResultMode.Projection);
            ((IRawGraphClient)client).ExecuteCypher(query1);
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Platform) where n.name = {platformName} detach delete n",
                                                            queryDict, CypherResultMode.Projection);
            ((IRawGraphClient)client).ExecuteCypher(query);
            MessageBox.Show("Deleted platform " + platform.name);
        }

        private void moreBtn_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                Game g = games[dataGridView1.SelectedRows[0].Index];
                var query = client.Cypher.Match("(g: Game { id: " + g.id + " })-[:AVAILABLE_ON]->(p: Platform)").Return<String>("p.name");
                var res = query.Results.ToList<String>();
                String more_about_a_game = "Game: " + g.title + " is available on " + res[0] + "\n";

                var query3 = client.Cypher.Match("(c: Company)-[:DEVELOPED]->(g: Game { id: " + g.id + " })").Return<String>("c.name");
                var res3 = query3.Results.ToList<String>();
                more_about_a_game += "\nDeveloped by: " + res3[0] + "\n";

                var query2 = client.Cypher.Match("(g: Game { id: " + g.id + " })-[:SIMILAR_WITH]->(p: Game)").ReturnDistinct<String>("p.title");
                var res2 = query2.Results.ToList<String>();
                if (res2.Count > 0) more_about_a_game += "\nSimilar games:\n\n";

                foreach (String st in res2)
                    more_about_a_game += "\t" + st + "\n";


                MessageBox.Show(more_about_a_game);
            }
            else
                MessageBox.Show("Kliknite na prvu kolonu reda za koji zelite da se prikaze");


        }

    }
}

