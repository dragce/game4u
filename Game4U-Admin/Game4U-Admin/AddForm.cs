﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game4U_Admin.DomainModel;
using Neo4jClient.Cypher;
using Neo4jClient;

namespace Game4U_Admin
{
    public partial class AddForm : Form
    {
        public GraphClient client;
        public String header;
        private List<Company> companies;
        private List<Platform> platforms;

        public AddForm()
        {
            InitializeComponent();
        }

        private void AddForm_Load(object sender, EventArgs e)
        {
            label1.Text = "Add " + header;
            if (header != "game") //ako nije izabrano iz menija add game
            {
                label3.Visible = false;
                label4.Visible = false;
                label5.Visible = false;
                label6.Visible = false;
                label7.Visible = false;
                label8.Visible = false;
                label9.Visible = false;
                label10.Visible = false;
                label11.Visible = false;
                label12.Visible = false;
                label13.Visible = false;

                textBox2.Visible = false;
                textBox3.Visible = false;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox8.Visible = false;
                textBox9.Visible = false;
                textBox10.Visible = false;

                comboBox1.Visible = false;
                richTextBox1.Visible = false;
                checkedListBox1.Visible = false;
                checkedListBox2.Visible = false;
                
                this.Height = 180;
                this.Width = 520;
              
            }

            //sledece funkcije popunjavaju polja koja se koriste pri kreiranju relacija cvora tipa Game
            fillPlatforms();
            fillGames();
            fillCompanies();
        }
        private void fillPlatforms()
        {
            checkedListBox1.CheckOnClick = true;
            var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Platform) RETURN n",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
          
            platforms = ((IRawGraphClient)client).ExecuteGetCypherResults<Platform>(query3).ToList();
            int i = 0;
            foreach (Platform p in platforms)
            {
                checkedListBox1.Items.Insert(i, p.name);
                i++;
            }
        }

        private void fillGames() //da popuni igrice za izbor onih koje su SIMILAR
        {
            checkedListBox2.CheckOnClick = true;
           
            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Game) RETURN DISTINCT n.title",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);
            List<String> games = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query2).ToList();
            int i = 0;
            foreach (String g in games)
            {
                checkedListBox2.Items.Insert(i, g);
                i++;
            }
            }

        private void fillCompanies() //da popuni moguce kompanije za izbor
        {
            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Company) RETURN n",
                                                         new Dictionary<string, object>(), CypherResultMode.Set);
           
            companies = ((IRawGraphClient)client).ExecuteGetCypherResults<Company>(query2).ToList();
            foreach (Company c in companies)
            {
                comboBox1.Items.Add(c.name);
            }
        }

        private void addCompany()
        {
            Company company = new Company();
            company.name = textBox1.Text;
            foreach (Company c in companies)
                if (company.name == c.name)
                {
                    MessageBox.Show("Company with that name already exists. You need to enter another name.");
                    return;
                }
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("cname", company.name);

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Company {name:{cname}})",
                                                            queryDict, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
            MessageBox.Show("Added " + company.name + " to the list of companies.");
            this.Close();
        }

        private void addPlatform()
        {
            Platform platform = new Platform();
            platform.name = textBox1.Text;
            foreach(Platform p in platforms)
                if(p.name==platform.name)
                {
                    MessageBox.Show("Platform with that name already exists. You need to enter another name.");
                    return;
                }
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("pname", platform.name);
            
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Platform {name:{pname}})",
                                                                       queryDict, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);
            MessageBox.Show("Added " + platform.name + " to the list of platforms.");
            this.Close();
        }

        private void addGame()
        {
            String developed = comboBox1.SelectedItem.ToString();
            Game game = new Game();
            game.title = textBox1.Text;
            game.image = textBox2.Text;
            game.quantity = Int32.Parse(textBox3.Text);
            game.price = Double.Parse(textBox4.Text);
            game.released = Int32.Parse(textBox5.Text);
            game.genre = textBox10.Text;
            game.rating = float.Parse(textBox9.Text);
            game.cover = textBox8.Text;
            game.synopsis = richTextBox1.Text;
            game.developedByCompany = comboBox1.Text;
            game.id = getMaxId() + 1;
            game.availablePlatform = checkedListBox1.CheckedItems[0].ToString();
            
            for (int i = 0; i < checkedListBox2.CheckedItems.Count; i++)
            {
                game.addSimilarGame(checkedListBox2.CheckedItems[i].ToString());
            }

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            
            queryDict.Add("id", game.id);
            queryDict.Add("name", game.title);
            queryDict.Add("image", game.image);
            queryDict.Add("quantity", game.quantity);
            queryDict.Add("price", game.price);
            queryDict.Add("released", game.released);
            queryDict.Add("genre", game.genre);
            queryDict.Add("rating", game.rating);
            queryDict.Add("cover", game.cover);
            queryDict.Add("synopsis", game.synopsis);
            queryDict.Add("dev",developed);
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Game {id:{id}, title:{name}, image:{image}, quantity:{quantity}, price:{price}, released:{released}, genre:{genre}, "
                                            +"rating:{rating}, cover:{cover}, synopsis:{synopsis} })",
                                                            queryDict, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query);

            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (g:Game {id: {id}}), (c:Company {name: {dev}}) CREATE (c)-[:DEVELOPED]->(g)",
            queryDict, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query2);
            
            var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Game{ id: {id} }), (p:Platform { name: '" + game.availablePlatform + "' }) CREATE(n)-[:AVAILABLE_ON]->(p)",
                                                           queryDict, CypherResultMode.Set);
            ((IRawGraphClient)client).ExecuteCypher(query3);
        
            foreach (String s in game.similarGames)
            {
                var query4 = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Game{ id: {id} }), (p:Game { title: '" + s + "' }) CREATE(n)-[:SIMILAR_WITH]->(p)",
                                                               queryDict, CypherResultMode.Set);
                ((IRawGraphClient)client).ExecuteCypher(query4);
            }
            MessageBox.Show("Added " + game.title + " to the list of games.");
            this.Close();
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        { //ova funkcija omogucava da se selektuje samo jedna platforma
            //preuzeto sa interneta
            if (e.NewValue == CheckState.Checked)
                for (int ix = 0; ix < checkedListBox1.Items.Count; ++ix)
                    if (e.Index != ix) checkedListBox1.SetItemChecked(ix, false);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (header == "company")
            {
                if(CheckCPinput())
                    addCompany();
                else
                    MessageBox.Show("Morate popuniti sva polja");
            }
            else if (header == "game")
            {
                if (checkGameInput())
                    addGame();
                else
                    MessageBox.Show("Morate popuniti sva polja");
            }
            else
            {
                if (CheckCPinput())
                    addPlatform();
                else
                    MessageBox.Show("Morate popuniti sva polja");
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private int getMaxId()
        { //funkcija preuzeta sa laba
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            string maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).ToList().FirstOrDefault();

            return Int32.Parse(maxId);
        }

        //za brisanje, ne koristi se
        private int getIntFromStr(String s)
        {
            String number="";
            int i = 0;
            while (i < s.Length && s[i] != '.') { 
                number += s[i];
                i++;
            }
            return Int32.Parse(number);
        }

       private bool checkGameInput()
        {
            bool a = false;

            if (textBox1.Text == "")
                return false;

            if (textBox2.Text == "")
                return false;

            if (textBox3.Text == "")
                return false;

            if (textBox4.Text == "")
                return false;

            if (textBox5.Text == "")
                return false;

            if (textBox8.Text == "")
                return false;

            if (textBox9.Text == "")
                return false;

            if (textBox10.Text == "")
                return false;

            if (comboBox1.SelectedItem == null)
                return false;

            if (richTextBox1.Text == "")
                return false;

            for(int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                    a = true;
            }

            if (a == false)
                return false;

            return true;
        }

        private bool CheckCPinput()
        {
            if (textBox1.Text == "")
                return false;
            else 
                return true;
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
