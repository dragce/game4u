﻿namespace Game4U_Admin
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.platformToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.platformToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.seeItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gameToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.platformToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.gameToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.platformToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addItemToolStripMenuItem,
            this.changeItemToolStripMenuItem,
            this.seeItemToolStripMenuItem,
            this.deleteItemToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addItemToolStripMenuItem
            // 
            this.addItemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.companyToolStripMenuItem,
            this.gameToolStripMenuItem,
            this.platformToolStripMenuItem});
            this.addItemToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.addItemToolStripMenuItem.Name = "addItemToolStripMenuItem";
            this.addItemToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.addItemToolStripMenuItem.Text = "Add ";
            // 
            // companyToolStripMenuItem
            // 
            this.companyToolStripMenuItem.Name = "companyToolStripMenuItem";
            this.companyToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.companyToolStripMenuItem.Text = "Company";
            this.companyToolStripMenuItem.Click += new System.EventHandler(this.companyToolStripMenuItem_Click);
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gameToolStripMenuItem.Text = "Game";
            this.gameToolStripMenuItem.Click += new System.EventHandler(this.gameToolStripMenuItem_Click);
            // 
            // platformToolStripMenuItem
            // 
            this.platformToolStripMenuItem.Name = "platformToolStripMenuItem";
            this.platformToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.platformToolStripMenuItem.Text = "Platform";
            this.platformToolStripMenuItem.Click += new System.EventHandler(this.platformToolStripMenuItem_Click);
            // 
            // changeItemToolStripMenuItem
            // 
            this.changeItemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.companyToolStripMenuItem1,
            this.gameToolStripMenuItem1,
            this.platformToolStripMenuItem1});
            this.changeItemToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.changeItemToolStripMenuItem.Name = "changeItemToolStripMenuItem";
            this.changeItemToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.changeItemToolStripMenuItem.Text = "Change";
            // 
            // companyToolStripMenuItem1
            // 
            this.companyToolStripMenuItem1.Name = "companyToolStripMenuItem1";
            this.companyToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.companyToolStripMenuItem1.Text = "Company";
            this.companyToolStripMenuItem1.Click += new System.EventHandler(this.companyToolStripMenuItem1_Click);
            // 
            // gameToolStripMenuItem1
            // 
            this.gameToolStripMenuItem1.Name = "gameToolStripMenuItem1";
            this.gameToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.gameToolStripMenuItem1.Text = "Game";
            this.gameToolStripMenuItem1.Click += new System.EventHandler(this.gameToolStripMenuItem1_Click);
            // 
            // platformToolStripMenuItem1
            // 
            this.platformToolStripMenuItem1.Name = "platformToolStripMenuItem1";
            this.platformToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.platformToolStripMenuItem1.Text = "Platform";
            this.platformToolStripMenuItem1.Click += new System.EventHandler(this.platformToolStripMenuItem1_Click);
            // 
            // seeItemToolStripMenuItem
            // 
            this.seeItemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.companyToolStripMenuItem2,
            this.gameToolStripMenuItem2,
            this.platformToolStripMenuItem2});
            this.seeItemToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.seeItemToolStripMenuItem.Name = "seeItemToolStripMenuItem";
            this.seeItemToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.seeItemToolStripMenuItem.Text = "Search";
            // 
            // companyToolStripMenuItem2
            // 
            this.companyToolStripMenuItem2.Name = "companyToolStripMenuItem2";
            this.companyToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.companyToolStripMenuItem2.Text = "Company";
            this.companyToolStripMenuItem2.Click += new System.EventHandler(this.companyToolStripMenuItem2_Click);
            // 
            // gameToolStripMenuItem2
            // 
            this.gameToolStripMenuItem2.Name = "gameToolStripMenuItem2";
            this.gameToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.gameToolStripMenuItem2.Text = "Game";
            this.gameToolStripMenuItem2.Click += new System.EventHandler(this.gameToolStripMenuItem2_Click);
            // 
            // platformToolStripMenuItem2
            // 
            this.platformToolStripMenuItem2.Name = "platformToolStripMenuItem2";
            this.platformToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.platformToolStripMenuItem2.Text = "Platform";
            this.platformToolStripMenuItem2.Click += new System.EventHandler(this.platformToolStripMenuItem2_Click);
            // 
            // deleteItemToolStripMenuItem
            // 
            this.deleteItemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.companyToolStripMenuItem3,
            this.gameToolStripMenuItem3,
            this.platformToolStripMenuItem3});
            this.deleteItemToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.deleteItemToolStripMenuItem.Name = "deleteItemToolStripMenuItem";
            this.deleteItemToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.deleteItemToolStripMenuItem.Text = "Delete";
            // 
            // companyToolStripMenuItem3
            // 
            this.companyToolStripMenuItem3.Name = "companyToolStripMenuItem3";
            this.companyToolStripMenuItem3.Size = new System.Drawing.Size(180, 22);
            this.companyToolStripMenuItem3.Text = "Company";
            this.companyToolStripMenuItem3.Click += new System.EventHandler(this.companyToolStripMenuItem3_Click);
            // 
            // gameToolStripMenuItem3
            // 
            this.gameToolStripMenuItem3.Name = "gameToolStripMenuItem3";
            this.gameToolStripMenuItem3.Size = new System.Drawing.Size(180, 22);
            this.gameToolStripMenuItem3.Text = "Game";
            this.gameToolStripMenuItem3.Click += new System.EventHandler(this.gameToolStripMenuItem3_Click);
            // 
            // platformToolStripMenuItem3
            // 
            this.platformToolStripMenuItem3.Name = "platformToolStripMenuItem3";
            this.platformToolStripMenuItem3.Size = new System.Drawing.Size(180, 22);
            this.platformToolStripMenuItem3.Text = "Platform";
            this.platformToolStripMenuItem3.Click += new System.EventHandler(this.platformToolStripMenuItem3_Click);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Navy;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "StartForm";
            this.Text = "Game4U";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seeItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem platformToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem platformToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem platformToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem platformToolStripMenuItem3;
    }
}

